<?php

include '../includes/DB.php';

$busca = "%".$_POST['busca']."%";

$mysqli = new mysqli($host, $user, $pass, $database);
$mysqli->query("SET NAMES 'utf8'");
$mysqli->query('SET character_set_connection=utf8');
$mysqli->query('SET character_set_client=utf8');
$mysqli->query('SET character_set_results=utf8');

$query = "select usuario.nick, usuario.nome, usuario.imagem from usuario where usuario.nick LIKE ? or usuario.Nome LIKE ?";
$stmt = $mysqli->prepare($query);
$stmt->bind_param('ss', $busca, $busca);
$stmt->execute();
$stmt->bind_result($nick, $nome, $imagem);
$stmt->store_result();

$response = array();

while ($stmt->fetch()){
    
    $vrau=array();
    $vrau["nick"]=$nick;
    $vrau["nome"]=$nome;
    $vrau["imagem"]=$imagem;
    array_push($response, $vrau);
}


echo json_encode($response);

$stmt->close();
$mysqli->close();