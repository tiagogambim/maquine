<?php

include '../includes/DB.php';
$action = $_POST['action'];

if ($action == "getnews") {

    $lastvrau = $_POST['lvrau'];
    $username = $_POST['username'];

    $mysqli = new mysqli($host, $user, $pass, $database);
    $mysqli->query("SET NAMES 'utf8'");
    $mysqli->query('SET character_set_connection=utf8');
    $mysqli->query('SET character_set_client=utf8');
    $mysqli->query('SET character_set_results=utf8');

    $query = "select v.Id_vrau, v.texto_vrau, v.imagem, U.nick, U.nome, U.imagem, v.data_vrau, ifnull((select count(*) from top where vrau_id=v.Id_vrau group by vrau_id), 0) as tops, ifnull((select 1 from top where vrau_id=Id_vrau and user_id=(Select id_user from usuario where nick=?)), 0) as deutop from vrau v, usuario U where U.id_user=v.Id_user and v.id_vrau>? order by 1 desc";
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param("si", $username, $lastvrau);
    $stmt->execute();
    $stmt->bind_result($id_vrau, $texto_vrau, $imagem_vrau, $nick, $nome, $imagem, $data_vrau, $topcount, $istop);
    $stmt->store_result();

    $response = array();

    while ($stmt->fetch()) {

        $data_vrau = $date = date_create($data_vrau);
        $data_vrau = date_format($data_vrau, 'd/m/Y H:i:s');

        $vrau = array();
        $vrau["id"] = $id_vrau;
        $vrau["text"] = $texto_vrau;
        $vrau["nick"] = $nick;
        $vrau["nome"] = $nome;
        $vrau["imagem"] = $imagem;
        $vrau["data"] = $data_vrau;
        if (!is_null($imagem_vrau)) {
            $vrau["imagemvrau"] = $imagem_vrau;
        } else {
            $vrau["imagemvrau"] = '';
        }
        $vrau["topcount"] = $topcount;
        $vrau["istop"] = $istop;
        array_push($response, $vrau);
    }


    echo json_encode($response);

    $stmt->close();
    $mysqli->close();
} else if ($action == "sendtop") {

    $vrauid = $_POST['vrauid'];
    $userid = $_POST['userid'];
    $option = $_POST['option'];

    $mysqli = new mysqli($host, $user, $pass, $database);
    $mysqli->query("SET NAMES 'utf8'");
    $mysqli->query('SET character_set_connection=utf8');
    $mysqli->query('SET character_set_client=utf8');
    $mysqli->query('SET character_set_results=utf8');

    $query = "call insere_top(?, ?, ?)";
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param('sii', $userid, $vrauid, $option);
    $stmt->execute();
    $stmt->close();
}