<?php
session_start();
include '../includes/DB.php';

$mysqli = new mysqli($host, $user, $pass, $database);

$login=$_SESSION['login'];

$nick = $_POST['nick'];
$senha =$_POST['senha'];
$nome = $_POST['nome'];


$uploaddir = '../profile-img/';
$filename = $_FILES['imagem']['name'];
$ext = pathinfo($filename, PATHINFO_EXTENSION);

$uploadfile = $uploaddir . $nick.".". $ext;

if (file_exists($uploadfile)) {
        unlink($uploadfile);
}

if (move_uploaded_file($_FILES['imagem']['tmp_name'], $uploadfile)) {
    echo "Arquivo válido e enviado com sucesso.\n";
} else {
    echo "Possível ataque de upload de arquivo!\n";
}

$sql = "update usuario set nick=?,  senha=md5(?),  nome=?, imagem=? where nick=?";

$stmt = $mysqli->prepare($sql);

$caminho = "profile-img/". $nick.".". $ext;
$stmt->bind_param('sssss', $nick, $senha, $nome, $caminho, $login);
$stmt->execute();

$stmt->close();
$mysqli->close();

$_SESSION['login'] = $nick;
header('Location: ../login');
?>