<?php
session_start();
if (!isset($_SESSION['login'])) {
    header('Location: login');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>PRoject Maquine</title>

        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">


        <style type="text/css" media="screen">
            body{
                background-color: #221157;
                font-family: 'Montserrat';
            }
            .login{
                background-color: white;
                margin-top: 20%;
                padding: 20px; 
            }
            .alo{
                width: 50%;
            }
            #blah{
                width: 100px;
            }
        </style>
    </head>
    <body>
        
        <?php
        include '../includes/DB.php';
        $mysqli = new mysqli($host, $user, $pass, $database);
        $sql = "select nome, nick from usuario where id_user=((Select Id_User from usuario where nick=?));";
        $stmt = $mysqli->prepare($sql);
        $nick = $_SESSION['login'];
        $stmt->bind_param('s', $nick);
        $stmt->execute();
        $stmt->bind_result($nome, $nick);
        $stmt->store_result();
        $stmt->fetch();
        $stmt->close();
        ?>

        <div class="container alo">
    <!--     <i id="acc" class="large material-icons prefix">person_pin</i> -->
            <form enctype="multipart/form-data" method="post" action="edit.php">
                <div class="login">
                    <div class="row">

                        <div class="input-field col s6 offset-s3">
                            <i class="material-icons prefix">account_circle</i>
                            <input name="nome" id="name" type="text" class="validate" placeholder="Name" value="<?php echo $nome ?>" required aria-required="true">
                            <!-- <label for="icon_prefix">User</label> -->
                            <label for="name" data-error="Preencha o campo" data-success="Correto"></label>
                        </div>
                        <div class="input-field col s6 offset-s3">
                            <i class="material-icons prefix">stars</i>
                            <input name="nick" id="nick" type="text" class="validate" placeholder="User" value="<?php echo $nick ?>"  required aria-required="true">
                            <label for="nick" data-error="Preencha o campo" data-success="Correto"></label>
                            <!-- <label for="icon_prefix">User</label> -->
                        </div>
                        <div class="input-field col s6 offset-s3">
                            <i class="material-icons prefix">lock</i>
                            <input name="senha" id="senha" type="password" class="validate" placeholder="Password" required aria-required="true">
                            <!-- <label for="icon_telephone">Password</label> -->
                            <label for="senha" data-error="Preencha o campo" data-success="Correto"></label>
                        </div>
                        <div class="input-field col s6 offset-s3">
                            <div class="file-field input-field">
                                <div class="btn">
                                    <span>File</span>
                                    <input required aria-required="true" name="imagem" type="file" id="imgInp">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="image">
                                </div>
                            </div>
                            <img src="acc.png" id="blah" alt="">
                        </div>
                    </div>
                </div>
                <button class="waves-effect waves-light btn right"><i class="material-icons right">save</i>save</button>
            </form>
            <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="../js/materialize.js"></script>
        <script src="../js/init.js"></script>
        <script src="../js/custom.js"></script>
        </div>
    </body>

</html>