var slideCont = 0;
var intervalo;
window.onload = startSlide();
function startSlide() {
    var setavolta = document.getElementById('setavolta');
    var setavai = document.getElementById('setaprox');
    var bolinha = document.getElementById('bolinhas');
    intervalo = setInterval(function () {
        slide(1);
    }, 3000);
    setavolta.addEventListener('click', slideBotao, null);
    setavai.addEventListener('click', slideBotao, null);
    bolinha.addEventListener('click', slideBolinha, null);
}

function slideBotao(evt) {
    valor = parseInt(event.target.id);    
    slide(valor);
    clearInterval(intervalo);
    intervalo = setInterval(function () {
        slide(1);
    }, 3000);
}

function slideBolinha(evt){
    valor = parseInt(event.target.id);    
    slide(valor - slideCont);
    clearInterval(intervalo);
    intervalo = setInterval(function () {
        slide(1);
    }, 3000);
}

function slide(valor) {
    var slides = document.getElementById('slider').getElementsByClassName('slide');
    var bolinhas = document.getElementById('bolinhas').getElementsByClassName('bolinhas');

    slides[slideCont].className = "slide";
    bolinhas[slideCont].className = "bolinhas";
    slideCont = slideCont + valor;
    if (slideCont == slides.length) {
        slideCont = 0;
    } else if (slideCont == -1) {
        slideCont = slides.length - 1;
    }
    slides[slideCont].className = "slide ativo";
    bolinhas[slideCont].className = "bolinhas ativo";

}