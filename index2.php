﻿<?php
session_start();
if (!isset($_SESSION['login'])) {
    header('Location: login');
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Project Maquine</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="css/materialize.css" type="text/css" rel="stylesheet" />
        <link href="css/style.css" type="text/css" rel="stylesheet"/>
        <link href="style.css" type="text/css" rel="stylesheet"/>
        <link href="css/mobile.css" type="text/css" rel="stylesheet" media="(max-width: 993px)" />
        <style>
            nav{
                background-color: white !important;
            }
            nav a,i{
                color: #221157 !important;
            }
        </style>

    </head>
    <body>
        <?php
        include 'includes/DB.php';
        $mysqli = new mysqli($host, $user, $pass, $database);
        $sql = "select imagem,  (select count(id_vrau) from vrau, (Select usuario.Id_User as idloro from usuario where nick=?) as consultavrau where vrau.id_user=idloro), (select count(id_call) from relacao_calls_user, (Select usuario.Id_User as idloro from usuario where nick=?) as consultavrau where relacao_calls_user.id_user=idloro), (select count(id_gt) from gt, (Select usuario.Id_User as idloro from usuario where nick=?) as consultavrau where gt.id_user=idloro) from usuario where nick=?";
        $stmt = $mysqli->prepare($sql);
        $nick = $_SESSION['login'];
        $stmt->bind_param('ssss', $nick, $nick, $nick, $nick);
        $stmt->execute();
        $stmt->bind_result($img, $count_vrau, $count_call, $count_gt);
        $stmt->store_result();
        $stmt->fetch();
        $stmt->close();
        ?>

        <div class="navbar-fixed">
                        
            <nav>
                <div class="nav-wrapper">
                    <ul class="left hide-on-med-and-down">
                        <li><a href="" id="back-to-top"><i class="material-icons">home</i></a></li>
                    </ul>
                    <ul class="right hide-on-med-and-down">
                        <!-- <li><a href="http://maquine.tiagogambim.com/"><i class="material-icons">view_module</i></a></li> -->
                        <li><a class="dropdown-button" href="http://maquine.tiagogambim.com/#!" data-activates="dropdown1"><?php echo $nick; ?><i class="material-icons right">arrow_drop_down</i></a>
                        <ul id="dropdown1" class="dropdown-content">
                            <li><a href="http://maquine.tiagogambim.com/profile/">Config</a></li>
                            <li class="divider"></li>
                            <li><a href="http://maquine.tiagogambim.com/logout.php">Logout</a></li>
                        </ul></li>
                    </ul>

                    <ul class="right hide-on-med-and-down">
                        <form>
                            <div class="input-field">
                            <input style="padding-top: 20px;" id="search" type="search" required>
                            <label for="search"><i class="material-icons">search</i></label>
                            <!-- <i class="material-icons">close</i> -->
                            </div>
                        </form>
                    </ul>
                </div>
            </nav>

        </div>

        <div class="">
            <div class="row todo">
                <div class="profile col s12 m12 l3">
                    <div class="row">
                        <div class="col s6">
                            <img class="img-responsive perfil" src="<?php echo $img ?>">
                        </div>
                        <div class="col s6 perfiltext">
                            <p perfiltext>@<?php echo $nick ?></p>
                            <p perfiltext><?php echo $count_vrau ?> vraus <br>
                                <?php echo $count_call ?> calls<br>
                                <?php echo $count_gt ?> GTS
                            </p>
                        </div>
                    </div>
                </div>
                <div class=" col s12 m12 l9">
                    <form method="post" action="post.php">
                        <div class="postar">
                            <div class="row marginzerob">

                                <div class="input-field col s12">
                                    <textarea id="textarea1" class="materialize-textarea" name="texto" maxlength="100" length="100"></textarea>
                                    <label for="textarea1">Digite algo</label>
                                </div>
                                <div class="col s8">
                                    <div class="col s4">
                                        <input value="vrau" checked="checked" class="with-gap" name="tipo" type="radio" id="test1" />
                                        <label for="test1">Vrau</label>
                                    </div>
                                    <div class="col s4">
                                        <input value="call" class="with-gap" name="tipo" type="radio" id="test2" />
                                        <label for="test2">Call</label>
                                    </div>
                                    <div class="col s4">
                                        <input value="call" class="with-gap" name="tipo" type="radio" id="test3"  />
                                        <label for="test3">GT</label>
                                    </div>
                                </div>
                                <button  class="col s2 offset-s2" id="vrau" type="submit">Vrau</button>
                            </div>
                        </div>
                    </form>
                    <div class="post row" id="newVrauAlert">
                        <div class="col m12 s12 center">
                            <p class="center" id="newVrauAlertText"></p>
                        </div>
                    </div>

                    <ul id="vrauList" class="marginzerot">

                        <?php
                        $query = "SELECT V.Id_vrau, V.texto_vrau, U.nick, U.nome, U.imagem, V.data_vrau FROM vrau V, usuario U where U.id_user=V.Id_user order by 1 desc";
                        $stmt = $mysqli->prepare($query);
                        $stmt->execute();
                        $stmt->bind_result($id_vrau, $texto_vrau, $nick, $nome, $imagem, $data_vrau);
                        $stmt->store_result();

                        
                        $lvrau=0;
                        $fvrau=-1;
                        
                        while ($stmt->fetch()) {
                            
                            $hashtags_no_vrau= substr_count(htmlspecialchars($texto_vrau, ENT_QUOTES), "#", 0, strlen(htmlspecialchars($texto_vrau, ENT_QUOTES)));
                            $array_posicoes = array();
                            $oie = 0;
                            $t6= "";
                            echo "<script>console.log('hashtags_no_vrau ".$hashtags_no_vrau."');</script>";
                            for($contador=0; $contador<$hashtags_no_vrau; $contador++){
                                $aff= strpos(htmlspecialchars($texto_vrau, ENT_QUOTES), "#", $oie );
                                array_push($array_posicoes, $aff);
                                $oie= $aff+1;
                                $sex=  strpos(htmlspecialchars($texto_vrau, ENT_QUOTES), " ", $oie);
                                
                            }
                            


                            $data_vrau = $date = date_create($data_vrau);
                            $data_vrau = date_format($data_vrau, 'd/m/Y H:i:s');
                            if($fvrau == -1){
                                $fvrau = $id_vrau;
                            }
                            if($fvrau > $id_vrau){
                                $fvrau = $id_vrau;
                            }
                            if($lvrau < $id_vrau){
                                $lvrau = $id_vrau;
                            }
                            
                            ?>
                            <li>
                                <div class="post row">
                                    <div class="gg col s1 m1 l2 center" ><img class="img-post" src="<?php echo $imagem; ?>" alt="" /></div>
                                    <div class="contentPost col s10 m10 l10">
                                        <p>@<?= $nick ?> (<?= $nome ?>)<span class="data"> - <?= $data_vrau ?></span></p> 
                                        <span class="conteudo" id="contentP">
                                            <?php
                                                for($contador=$hashtags_no_vrau; $contador>0; $contador--){
                                                    echo $array_posicoes[$contador];
                                                }

                                            ?>
                                            <!--?= substr_replace(htmlspecialchars($texto_vrau, ENT_QUOTES), "", 0, 0) ?==>
                                            <!--?= str_replace('>', '&rsaquo;', str_replace('<', '&lsaquo;', $texto_vrau)) ?-->
                                        </span>
                                    </div>
                                </div>
                            </li>

                            <?php
                        }
                        echo "<script>var lvrau = ".$lvrau.";</script>";
                        ?>

                    </ul>

                </div>
            </div>
        </div>    
        <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="js/materialize.js"></script>
        <script src="js/init.js"></script>
        <script src="js/custom.js"></script>
        <script>
            var outputHTML = "";
            var outputHTMLOld = "";
            var newVrauCont = 0;
            
            $(function () {
                $("#newVrauAlert").hide();
                setInterval(newvraus, 10000);
                //alert('hi');
            })
            function newvraus() {
                $.ajax({
                    type: "GET",
                    url: "ajaxfunctions/newfeed.php",
                    dataType: 'json',
                    data: {
                        lvrau: lvrau
                    },
                    success: function (result) {

                        //alert("Resultado Ajax");
                        newVrauCont += result.length;
                        
                        outputHTMLOld = outputHTML;
                        //alert(newVrauCont);
                        //alert(result);
                        if (newVrauCont > 0) {
                            document.title = "Maquiné("+newVrauCont+")"; 
                            //alert("Dentro if");
                            //var tweets = twitterFeed.statuses;
                            // Begin HTML wrapper

                            $("#newVrauAlertText").text(newVrauCont + ' novos vraus');
                            $("#newVrauAlert").show();

                            outputHTML="";

                            // Loop through JSON data and populate into HTML list
                            for (var i = 0; i < result.length; i++) {
                                outputHTML += '<li>';
                                outputHTML += '<div class="post row">';
                                // Declare variables for required data
                                var vrauId = result[i].id;
                                var vrauText = result[i].text;
                                vrauText = vrauText.replace('>', '&rsaquo;');
                                vrauText = vrauText.replace('<', '&lsaquo;');
                                var vrauNick = result[i].nick;
                                var vrauName = result[i].nome;
                                var vrauImg = result[i].imagem;
                                var vrauDate = result[i].data;

                                if(i== 0){
                                    console.log(vrauId);
                                    lvrau = vrauId;
                                }
                                // HTML magic
                                outputHTML += '<div class="gg col s2" ><img class="img-post" src="' + vrauImg + '" alt="" /></div>';
                                outputHTML += '<div class="contentPost col s9">';
                                outputHTML += '<p>@' + vrauNick + ' (' + vrauName + ')<span class="data"> - '+vrauDate+'</span></p>';
                                outputHTML += '<p id="contentP">';
                                outputHTML += vrauText;
                                outputHTML += '</p>';
                                outputHTML += '</div>';
                                outputHTML += '</div>';

                                // Get the length of tweet and truncate & ellipse if too long
                                /*
                                 if (tweetText.length > 72) {
                                 var ellipsisText = tweetText.substring(0, 72) + "...";
                                 outputHTML += "<div>" + ellipsisText + "</div>"
                                 } else {
                                 outputHTML += "<div>" + tweetText + "</div>"
                                 }
                                 */
                                outputHTML += "</li>";
                                
                            }
                            outputHTML += outputHTMLOld;

                            //$("#vrauList").prepend(outputHTML);
                        }else{
                            document.title = "Maquiné";
                        }
                    }
                });
            }
            $("#newVrauAlert").click(function () {
                //alert("Disparou evento click");
                $("#vrauList").prepend(outputHTML);
                $("#newVrauAlert").hide();
                newVrauCont = 0;
                outputHTMLOld = "";
                outputHTML = "";
                document.title = "Maquiné";
            });

            if ($('#back-to-top').length) {
                var scrollTrigger = 100, // px
                    backToTop = function () {
                        var scrollTop = $(window).scrollTop();
                        if (scrollTop > scrollTrigger) {
                            $('#back-to-top').addClass('show');
                        } else {
                            $('#back-to-top').removeClass('show');
                        }
                    };
                backToTop();
                $(window).on('scroll', function () {
                    backToTop();
                });
                $('#back-to-top').on('click', function (e) {
                    e.preventDefault();
                    $('html,body').animate({
                        scrollTop: 0
                    }, 700);
                });
            }

        </script>
        <style>
            #newVrauAlert:hover{
                background-color: rgba(255,255,255,0.4) !important;
                transition: 0.2s ease-out;
            }
</style>
    </body>
</html>
<?php
// tira o resultado da busca da memória
$stmt->close();
?>