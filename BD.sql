
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 25/01/2017 às 17:00:57
-- Versão do Servidor: 10.0.28-MariaDB
-- Versão do PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `u274737048_maqui`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `calls`
--

CREATE TABLE IF NOT EXISTS `calls` (
  `Id_Call` int(11) NOT NULL AUTO_INCREMENT,
  `Descricao_Call` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `data_Call` datetime DEFAULT NULL,
  `imagem` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nome_call` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_Criador` int(11) NOT NULL,
  PRIMARY KEY (`Id_Call`),
  KEY `id_Criador` (`id_Criador`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `gt`
--

CREATE TABLE IF NOT EXISTS `gt` (
  `Id_GT` int(11) NOT NULL AUTO_INCREMENT,
  `Texto_GT` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `data_GT` datetime DEFAULT NULL,
  `imagem` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`Id_GT`),
  KEY `id_user` (`id_user`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `hashtags`
--

CREATE TABLE IF NOT EXISTS `hashtags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `cont` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `relacao_calls_user`
--

CREATE TABLE IF NOT EXISTS `relacao_calls_user` (
  `Id_Relacao` int(11) NOT NULL AUTO_INCREMENT,
  `Id_User` int(11) NOT NULL,
  `Id_Call` int(11) NOT NULL,
  PRIMARY KEY (`Id_Relacao`),
  KEY `Id_User` (`Id_User`),
  KEY `Id_Call` (`Id_Call`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `Id_User` int(11) NOT NULL AUTO_INCREMENT,
  `nick` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `senha` varchar(33) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Nome` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'profile/acc.png',
  PRIMARY KEY (`Id_User`),
  FULLTEXT KEY `imagem` (`imagem`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`Id_User`, `nick`, `senha`, `Nome`, `imagem`) VALUES
(1, 'meupaudeasas', 'f3300f0e5466328b7e60c962a219b3f6', 'Tiago Gambim', 'profile-img/meupaudeasas.png'),
(2, 'Grackys', 'd492518452b36938271d355205e21d69', 'Matheus Madeira', 'profile/acc.png'),
(3, 'indio', '202cb962ac59075b964b07152d234b70', 'Lucas', 'profile-img/indio.jpg'),
(7, 'Brozagui', 'df969db0e01b0b21c82b63db10d51dc2', 'Andr', 'profile/acc.png'),
(8, 'luis_sobotyk', '21232f297a57a5a743894a0e4a801fc3', 'Luis Mauro Garcia Sobotyk', 'profile-img/luis_sobotyk.png'),
(9, 'Brobinho', '2b76221d7da68791561f2a31e87cc148', 'Robson Rodrigues', 'profile-img/Brobinho.png'),
(14, 'luis.sobotyk', 'c93ccd78b2076528346216b3b2f701e6', 'Luis 2.0', 'profile-img/luis.sobotyk.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vrau`
--

CREATE TABLE IF NOT EXISTS `vrau` (
  `Id_vrau` int(11) NOT NULL AUTO_INCREMENT,
  `Texto_vrau` varchar(140) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_vrau` datetime DEFAULT NULL,
  `imagem` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`Id_vrau`),
  KEY `id_user` (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=78 ;

--
-- Extraindo dados da tabela `vrau`
--

INSERT INTO `vrau` (`Id_vrau`, `Texto_vrau`, `data_vrau`, `imagem`, `id_user`) VALUES
(1, 'funcionando again', '2016-06-17 06:23:31', NULL, 1),
(2, 'vamos dar a bunda', '2016-06-17 06:25:07', NULL, 2),
(3, 'and here we go again :D', '2016-06-17 07:30:27', NULL, 3),
(4, 'e houve boatos q eu tava na pior', '2016-06-17 07:38:08', NULL, 3),
(5, 'that''s how we live, BITCH', '2016-06-17 11:47:31', NULL, 3),
(6, 'eu digito ''ma'' e so o q vem eh maquine.tiagogambim.com.br :''(', '2016-06-18 02:37:29', NULL, 3),
(7, 'rito gomes da ban perma nesse esy.es', '2016-06-18 02:38:07', NULL, 3),
(8, 'se pa meu gato acha q eu sou sujo p krl pq ele nunca ve eu me lambendo', '2016-06-18 03:04:59', NULL, 3),
(9, 'encontre o bug http://prntscr.com/bih6pr', '2016-06-19 08:49:20', NULL, 1),
(10, 'comprar duas espadas e sair por aí caçando monstros', '2016-06-19 08:51:23', NULL, 1),
(11, 'pnc de todos vcs seus drogado', '2016-06-19 08:58:59', NULL, 7),
(12, 'ta pra nascer alguém mais amorzinho que a mia khalifa', '2016-06-20 02:38:38', NULL, 7),
(13, 'THAT''S HOW WE LIVE, SWEETHEART', '2016-06-20 02:45:55', NULL, 3),
(14, 'Queria ser o Gracky, mas o Gracky não posso ser. O Gracky só pensa em morrer eu só penso em você!', '2016-06-20 06:54:32', NULL, 8),
(15, 'foice o tempo que o clan metia uns agito em maquine', '2016-06-20 10:15:51', NULL, 7),
(16, 'o meu, relaxa, vamos fazer bombar dnv', '2016-06-21 12:16:27', NULL, 8),
(17, 'olha a novinha q n me queria agora quer quer quer', '2016-06-21 02:45:15', NULL, 1),
(18, 'é q agr a 9vinha me qr pq virei morador da cidade', '2016-06-21 02:47:32', NULL, 1),
(19, '', '2016-06-21 02:49:46', NULL, 1),
(20, 'quem n sabe jovem', '2016-06-21 02:50:36', NULL, 7),
(21, 'ola ja assistiram mr. robot', '2016-06-21 04:30:41', NULL, 3),
(22, 'mr. robot mt topper', '2016-06-21 08:54:40', NULL, 1),
(23, 'gt amanhã só espera', '2016-06-21 09:04:36', NULL, 8),
(24, 'agr sim poha', '2016-06-21 09:05:04', NULL, 9),
(25, 'agora somos os barbixas', '2016-06-21 09:27:05', NULL, 7),
(26, 'por que tudo que é coisa fofinha as pessoas matam?', '2016-06-21 11:52:40', NULL, 3),
(27, 'maquine feels', '2016-06-22 03:53:27', NULL, 8),
(28, 'Rip Maquiné', '2016-06-24 02:50:51', NULL, 1),
(29, 'rip o caralho \r\nquem ta &quot;rip&quot; é o tiago em casa aopskposaksapks\r\n', '2016-06-25 08:10:04', NULL, 9),
(30, 'queria mandar um salve pro desconhecido q comprou uma água p mim', '2016-06-26 11:26:31', NULL, 1),
(31, 'um salve pro clan q me desceu lá de cima, me carregou até em casa e me botou na cama p dormir', '2016-06-26 11:27:01', NULL, 1),
(32, 'vcs são loros', '2016-06-26 11:27:21', NULL, 1),
(33, 'Nusss, Tiaguinho delícia foi o melhor bêbado da festa. Tu é demais s2', '2016-06-27 06:59:06', NULL, 9),
(34, 'S2', '2016-06-27 09:03:23', NULL, 1),
(35, 'Robinho falou o melhor, to imaginando vários bebados dando pt e eu me destacando dkpsakpdpkaspokdoak', '2016-06-27 09:03:53', NULL, 1),
(36, 'a famosa Edmaura', '2016-06-27 11:47:16', NULL, 1),
(37, 'aoskpoaskopakasopa, tipo isso @meupaudeasas', '2016-06-28 01:59:07', NULL, 9),
(38, 'EAI PORRA CARALHO', '2016-06-29 01:03:21', NULL, 3),
(39, 'e ai galera', '2016-07-01 05:07:59', NULL, 8),
(40, 'olar, aqui quem fala eh indio, direto de plante atlandida alo alo molieres', '2016-07-01 10:07:54', NULL, 3),
(41, 'essa poha morreu msm????', '2016-07-06 12:18:04', NULL, 9),
(42, 'esperto é o cara que organiza o que já existe e cria uma coisa nova.', '2016-07-06 06:37:24', NULL, 3),
(43, 'ja dizia sergio, enovar', '2016-07-06 06:37:51', NULL, 3),
(44, 'amém.', '2016-07-06 06:38:05', NULL, 3),
(45, 'Como ja dizia o indio, o onde come 1 come a aldeia td', '2016-07-08 05:28:59', NULL, 1),
(46, 'aospkoaspkaopskasopk', '2016-07-13 12:16:38', NULL, 9),
(47, 'full atrasado eu tlg ;-;  aopkopsaksa\r\n', '2016-07-13 12:16:49', NULL, 9),
(48, 'Robinho apaga essa rola', '2016-07-27 11:28:43', NULL, 8),
(49, 'maquiné é um lugar seguro novamente', '2016-07-27 11:31:33', NULL, 1),
(50, 'niceeee', '2016-08-12 12:12:49', NULL, 3),
(51, 'oi SEUS CU', '2016-08-30 08:36:12', NULL, 3),
(52, 'objetivo de vida é chegar aos 20 vraus', '2016-08-30 08:37:36', NULL, 3),
(53, 'to quase lá, faltam 3', '2016-09-01 08:58:31', NULL, 1),
(54, 'Robinho gosta tanto de mandar nude q manda até pros bro', '2016-09-02 10:28:10', NULL, 1),
(55, 'BAH GURIZADA TO TRISTE CADE VCS NESSES SITE :(', '2016-09-26 12:39:54', NULL, 3),
(56, 'o inverno é tudo CU\r\n', '2016-09-26 12:53:32', NULL, 3),
(57, 'ei rapeize, tudo certo p hj?', '2016-09-26 01:02:13', NULL, 3),
(58, 'Gente, amo vcs, mesmo q vcs n saibam disso. Quero ver a felicidade de todos. &lt;3', '2016-09-27 01:04:21', NULL, 9),
(59, 'NA site', '2016-09-30 10:15:47', NULL, 1),
(60, 'NA dia', '2016-09-30 10:15:57', NULL, 1),
(61, 'NA vida', '2016-09-30 10:16:07', NULL, 1),
(62, 'BAH TIPO AMO VCS BEM GOSTOSO', '2016-10-03 08:06:50', NULL, 3),
(63, 'GOSTOSO É MEU PAU!!!!!!!!!1\r\n', '2016-10-03 08:07:04', NULL, 9),
(64, 'será q é perigoso pular sarrando do 2 andar?', '2016-10-03 08:09:05', NULL, 1),
(65, 'O MEU PF VAMO INCREMETAR ESSE SITE TIPO LIKE  E RESPOMDERR PLSSS', '2016-10-03 08:10:39', NULL, 3),
(66, 'João lindo', '2016-10-03 08:18:13', NULL, 9),
(67, 'lista de mulheres lindas:\r\nsjokz,\r\nfleeur,\r\nmel fronckowiak', '2016-10-03 08:20:10', NULL, 3),
(68, 'SE CASO ALGUM DE VCS LER ISSO AQ, POR FAVOR PEÇO UMA COISA: N DEIXA EU BEBE PINGA', '2016-10-22 04:52:38', NULL, 3),
(69, 'quase fui atropelado', '2016-11-28 03:39:40', NULL, 1),
(70, 'qualquer', '2017-01-03 02:07:11', NULL, 11),
(71, 'aaaaaaaaah', '2017-01-03 02:07:37', NULL, 12),
(72, 'MELHOR SITE NAMORAL', '2017-01-04 11:30:25', NULL, 3),
(73, 'carai q ppoha eh essa vanderlei', '2017-01-04 11:34:56', NULL, 3),
(76, 'seus chera cu', '2017-01-23 02:35:57', NULL, 14),
(77, 'Vou fazer o editor de GT porra', '2017-01-23 03:18:52', NULL, 14);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
