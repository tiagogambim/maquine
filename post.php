<?php
session_start();
include './includes/DB.php';
date_default_timezone_set('America/Sao_Paulo');
$mysqli = new mysqli($host, $user, $pass, $database);
$tipopost = $_POST['tipo'];
$nick = $_SESSION['login'];

if($tipopost == "call"){
    $titulo = htmlspecialchars($_POST['titulocall']);
    $local = htmlspecialchars($_POST['localcall']);
    $dataPost= date('Y-m-d h:i:s');
    $dataCall = $_POST['datacall'];
    $horaCall = $_POST['horacall'];

    $DFm = explode("/",$dataCall);
    $dataComp= $dataCall." ".$horaCall.":00";

    $descricao = nl2br(htmlspecialchars($_POST['descricaocall']));

    $sql= "insert into calls (Descricao_Call, data_Call, nome_call, id_Criador, local_call, dataPostagem_call) values (?, ?, ?, (select Id_User from usuario where nick=?), ?, ?);";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param('ssssss', $descricao, $dataComp, $titulo, $nick, $local, $dataPost);
    $stmt->execute();
    $id = $stmt->insert_id;
    $stmt->close();

    if(isset($_FILES['imagem'])){
        //se a imagem ta setada
        $uploaddir = 'img_calls/';
        $filename= $_FILES['imagem']['name'];
        $ext= pathinfo($filename, PATHINFO_EXTENSION);
        $uploadfile = $uploaddir . $id.".".$ext;
        //cria o arquivo c nome = id
        if(file_exists($uploadfile)){
            unlink($uploadfile);
        }
        if(move_uploaded_file($_FILES['imagem']['tmp_name'], $uploadfile)){
            //se rolou copiar p pasta faz update
            $sql3 = "update calls set imagem=? where Id_Call=?";
            $stmt = $mysqli->prepare($sql3);
            $stmt->bind_param('si', $uploadfile, $id);
            $stmt->execute();
            $stmt->close();
        } 
    }

    $sql2= "INSERT into relacao_calls_user (Id_User, Id_Call) values ((select Id_User from usuario where nick=?), ?)";
    $stmt = $mysqli->prepare($sql2);
    $stmt->bind_param('si', $nick, $id);
    $stmt->execute();
    $id = $stmt->insert_id;
    $stmt->close(); 
    
}else if($tipopost == "gt"){
    //insere no banco
    $texto = htmlspecialchars($_POST['texto']);
    $data= date('Y-m-d h:i:s');
    $sql = "insert into gt (Id_GT, Texto_GT, data_GT, id_user) values (?, ?, ?, (Select Id_User from usuario where nick=?))";
    $stmt = $mysqli->prepare($sql);
    $texto = nl2br($texto);
    $stmt->bind_param('isss', $gtzao, $texto, $data, $nick);
    $stmt->execute();
    //salva o id armazenado
    $id = $stmt->insert_id;
    $stmt->close();
    
    if(isset($_FILES['imagem'])){
        //se a imagem ta setada
        $uploaddir = 'img_gt/';
        $filename= $_FILES['imagem']['name'];
        $ext= pathinfo($filename, PATHINFO_EXTENSION);
        $uploadfile = $uploaddir . $id.".".$ext;
        //cria o arquivo c nome = id
        if(file_exists($uploadfile)){
            unlink($uploadfile);
        }
        if(move_uploaded_file($_FILES['imagem']['tmp_name'], $uploadfile)){
            //se rolou copiar p pasta faz update
            $sql = "update gt set imagem=? where Id_GT=?";
            $stmt = $mysqli->prepare($sql);
            $stmt->bind_param('si', $uploadfile, $id);
            $stmt->execute();
            $stmt->close();
        } 
    }
    
    

}else if($tipopost == "vrau"){
    $texto = htmlspecialchars($_POST['texto']);
    $data= date('Y-m-d h:i:s');
    $sql = "insert into vrau (Texto_vrau, data_vrau, id_user) values (?, ?, (Select Id_User from usuario where nick=?))";
    $stmt = $mysqli->prepare($sql);
    $texto = nl2br($texto);
    $stmt->bind_param('sss', $texto, $data, $nick);
    $stmt->execute();
    //salva o id armazenado
    $id = $stmt->insert_id;
    $stmt->close();
    
    if(isset($_FILES['imagem'])){
        //se a imagem ta setada
        $uploaddir = 'img_vrau/';
        $filename= $_FILES['imagem']['name'];
        $ext= pathinfo($filename, PATHINFO_EXTENSION);
        $uploadfile = $uploaddir . $id.".".$ext;
        //cria o arquivo c nome = id
        if(file_exists($uploadfile)){
            unlink($uploadfile);
        }
        if(move_uploaded_file($_FILES['imagem']['tmp_name'], $uploadfile)){
            //se rolou copiar p pasta faz update
            $sql = "update vrau set imagem=? where Id_vrau=?";
            $stmt = $mysqli->prepare($sql);
            $stmt->bind_param('si', $uploadfile, $id);
            $stmt->execute();
            $stmt->close();
        } 
    }
    
    
    
    $arrlinhas = explode(" ", $texto);
    foreach ($arrlinhas as $linha){
        if($linha[0]=='#'){
            $sql2 = "INSERT INTO hashtags (texto) SELECT ? t from hashtags tmp WHERE NOT EXISTS (SELECT texto FROM hashtags WHERE texto = ?) LIMIT 1";
            $stmt2 = $mysqli->prepare($sql2);
            $stmt2->bind_param('ss', $linha, $linha);
            $stmt2->execute();
            $stmt2->close();
            
            $sql3 = "update hashtags set hashtags.cont=hashtags.cont+1 where hashtags.texto=?";
            $stmt3 = $mysqli->prepare($sql3);
            $stmt3->bind_param('s', $linha);
            $stmt3->execute();
            $stmt3->close();
        }
    }

}
header('Location: index.php');