<?php
session_start();
if (!isset($_SESSION['login'])) {
    header('Location: login');
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Project Maquine</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="css/materialize.css" type="text/css" rel="stylesheet" />
        <link href="css/style.css" type="text/css" rel="stylesheet"/>
        <!--link href="style.css" type="text/css" rel="stylesheet"/-->
        <link href="css/mobile.css" type="text/css" rel="stylesheet" media="(max-width: 993px)" />
        <style>
            nav{
                background-color: white !important;
            }
            nav a,i{
                color: #221157 !important;
            }
            #newVrauAlert:hover{
                background-color: rgba(255,255,255,0.4) !important;
                transition: 0.2s ease-out;
            }
        </style>

    </head>
    <body>
        <?php
        include 'includes/DB.php';
        $mysqli = new mysqli($host, $user, $pass, $database);
        $sql = "select imagem, nome, (select count(id_vrau) from vrau, (Select usuario.Id_User as idloro from usuario where nick=?) as consultavrau where vrau.id_user=idloro), (select count(id_call) from relacao_calls_user, (Select usuario.Id_User as idloro from usuario where nick=?) as consultavrau where relacao_calls_user.id_user=idloro), (select count(id_gt) from gt, (Select usuario.Id_User as idloro from usuario where nick=?) as consultavrau where gt.id_user=idloro) from usuario where nick=?";
        $stmt = $mysqli->prepare($sql);
        $nick = $_SESSION['login'];
        $stmt->bind_param('ssss', $nick, $nick, $nick, $nick);
        $stmt->execute();
        $stmt->bind_result($img, $nomehuehue, $count_vrau, $count_call, $count_gt);
        $stmt->store_result();
        $stmt->fetch();
        $stmt->close();
        ?>

        <div class="navbar-fixed">

            <nav>
                <div class="nav-wrapper">
                    <ul class="left hide-on-med-and-down">
                        <li><a href="" id="back-to-top"><i class="material-icons">home</i></a></li>
                    </ul>
                    <ul class="right hide-on-med-and-down">
                        <!-- <li><a href="http://maquine.tiagogambim.com/"><i class="material-icons">view_module</i></a></li> -->
                        <li><a class="dropdown-button" href="#!" data-activates="dropdown1"><?php echo $nick; ?><i class="material-icons right">arrow_drop_down</i></a>
                            <ul id="dropdown1" class="dropdown-content">
                                <li><a href="profile/">Config</a></li>
                                <li class="divider"></li>
                                <li><a href="logout.php">Logout</a></li>
                            </ul></li>
                    </ul>

                    <ul class="right hide-on-med-and-down">
                        <form>
                            <div class="input-field">
                                <input name="busca" class="dropdown-button" data-beloworigin="true" style="padding-top: 20px;" id="busca" type="search" data-activates='dropdown2' required>
                                <label for="busca"><i class="material-icons">search</i></label>
                                <!-- <i class="material-icons">close</i> -->
                                <!-- Dropdown Structure -->
                                <ul id='dropdown2' class='dropdown-content'>

                                </ul>
                            </div>
                        </form>
                    </ul>
                </div>
            </nav>

        </div>

        <div class="">
            <div class="row todo">
                <div class="profile col s12 m12 l3">
                    <div class="row valign-wrapper">
                        <div class="col s6">
                            <img class="img-responsive perfil" src="<?php echo $img ?>">
                        </div>
                        <div class="col s6 perfiltext">
                            <p perfiltext><?php echo $nomehuehue ?> <br>
                            @<?php echo $nick ?></p>
                            <p perfiltext><?php echo $count_vrau ?> vraus <br>
                                <?php echo $count_call ?> calls<br>
                                <?php echo $count_gt ?> GT'S
                            </p>
                        </div>
                    </div>
                </div>
                <div class=" col s12 m12 l9">
                    <form enctype="multipart/form-data" method="post" action="post.php">

                        <div class="postar">
                            <div class="row marginzerob">
                                <div class="row">
                                
                                <!-- DEIXEI 2 DIVS SETADAS, UMA PRA QUANDO NAO EH CALL E UMA PRA QUANDO É CALL, COM INPUTS DIFERENTES -->

                                <div class="input-field col s12" id="noncall" style="display: block">
                                    <textarea id="textarea1" class="materialize-textarea" name="texto" maxlength="100" length="100" required></textarea>
                                    <label for="textarea1">Digite algo</label>
                                </div>


                                <!-- Inicio da div de calls -->

                                <div class="input-field col s12" id="callInputs" style="display: none">
                                    
                                    <div class="row col s12">
                                        <div class="col s6">
                                            <input name="titulocall" class="chupaminharola" id="titulocall" type="text" class="validate" placeholder="Titulo da Call" >
                                            <label for="titulocall" data-error="Preencha o campo" data-success="Correto"></label>
                                        </div>
                                        <div class="col s6">
                                            <input name="localcall" class="chupaminharola" id="localcall" type="text" class="validate" placeholder="Local da Call" >
                                            <label for="localcall" data-error="Preencha o campo" data-success="Correto"></label>
                                        </div>
                                    </div>
                                    <div class="row col s12">
                                        <div class="col s3">
                                            <input name="datacall" class="chupaminharola" id="datacall" type="date" class="validate" placeholder="Data da Call" >
                                            <label for="datacall" data-error="Preencha o campo" data-success="Correto"></label>
                                        </div>
                                        <div class="col s3">
                                            <input name="horacall" class="chupaminharola" id="horacall" type="time" class="validate" placeholder="Horário da Call" >
                                            <label for="horacall" data-error="Preencha o campo" data-success="Correto"></label>
                                        </div>
                                        <div class="col s6">
                                        <textarea id="descricaocall" class="materialize-textarea" name="descricaocall" placeholder="Mais Informações da Call"></textarea>
                                        <label for="descricaocall" data-error="Preencha o campo" data-success="Correto"></label>
                                        </div>
                                    </div>


                                    <!--table class="input-field col s12">
                                    <tr>
                                        <td>
                                            <input name="titulocall" id="titulocall" type="text" class="validate" placeholder="Titulo da Call" required >
                                            <label for="titulocall" data-error="Preencha o campo" data-success="Correto"></label>
                                        </td>
                                        <td>
                                            <input name="datacall" id="datecall" type="date" class="validate" placeholder="Titulo da Call" required >
                                            <label for="titulocall" data-error="Preencha o campo" data-success="Correto"></label>
                                        </td>
                                    </tr>
                                    </table-->


                                </div>

                                <!-- Fim da div de calls -->

                                <div class="col s8">
                                    <div class="col s4">
                                        <input value="vrau" checked="checked" class="with-gap" name="tipo" type="radio" id="test1" onclick="cabritinha()" />
                                        <label for="test1">Vrau</label>
                                    </div>
                                    <div class="col s4">
                                        <input value="call" class="with-gap" name="tipo" type="radio" id="test2" onclick="cabritinha()" />
                                        <label for="test2">Call</label>
                                    </div>
                                    <div class="col s4">
                                        <input value="gt" class="with-gap" name="tipo" type="radio" id="test3" onclick="cabritinha()" />
                                        <label for="test3">GT</label>
                                    </div>
                                </div>
                                </div>
                                <div class="row valign-wrapper">
                                <div class="col s1">
                                <!-- chupa -->
                                <div class="file-field input-field" style="margin: auto;">
                                    <div class="btn col s12" style="padding-top: 0.4rem; background: #A62649;">
                                        <span class="large material-icons center">perm_media</span>
                                        <input name="imagem" type="file" id="imgInp">
                                    </div>
                                    <!--
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="image">
                                </div>
                                    -->

                                </div>
                                </div>
                                    <button  class="col s2 offset-s9" id="vrau" type="submit">Vrau</button>
                                </div>

                            </div>
                        </div>
                    </form>
                    <div class="post row" id="newVrauAlert">
                        <div class="col m12 s12 center">
                            <p class="center" id="newVrauAlertText"></p>
                        </div>
                    </div>

                    <br />

                    <ul id="vrauList" class="marginzerot">

                        <?php
                        $query = "select v.Id_vrau, v.texto_vrau, v.imagem, U.nick, U.nome, U.imagem, v.data_vrau, ifnull((select count(*) from top where vrau_id=v.Id_vrau group by vrau_id), 0) as tops, ifnull((select 1 from top where vrau_id=Id_vrau and user_id=(Select id_user from usuario where nick=?)), 0) as deutop from vrau v, usuario U where U.id_user=v.Id_user order by 1 desc";
                        $stmt = $mysqli->prepare($query);
                        $stmt->bind_param("s", $_SESSION['login']);
                        $stmt->execute();
                        $stmt->bind_result($id_vrau, $texto_vrau, $imagem_vrau, $nick, $nome, $imagem, $data_vrau, $topcount, $istop);
                        $stmt->store_result();


                        $lvrau = 0;
                        $fvrau = -1;
                        while ($stmt->fetch()) {
                            $data_vrau = $date = date_create($data_vrau);
                            $data_vrau = date_format($data_vrau, 'd/m/Y H:i:s');
                            if ($fvrau == -1) {
                                $fvrau = $id_vrau;
                            }
                            if ($fvrau > $id_vrau) {
                                $fvrau = $id_vrau;
                            }
                            if ($lvrau < $id_vrau) {
                                $lvrau = $id_vrau;
                            }
                            ?>
                            <li>
                                <div class="post row">
                                    <div class="col s2 center" ><img class="img-post" src="<?php echo $imagem; ?>" alt="" /></div>
                                    <div class="contentPost col s10 m10 l10">
                                        <div class="row valign-wrapper">
                                        <div class="col s11">
                                        <p>@<?= $nick ?> (<?= $nome ?>)<span class="data"> - <?= $data_vrau ?></span></p> 
                                        <span class="conteudo row" id="contentP">
                                            <div class="col s12"><?= $texto_vrau ?></div>
                                            <div class="col s12">
                                            <?php 
                                                if(!is_null($imagem_vrau)){
                                                    echo "<img class='materialboxed' src='".$imagem_vrau."' style='width: auto;'>";
                                                }
                                            ?>
                                            </div>
                                        </span>
                                        </div>
                                        <div class="col s1" style="height: 100%">
                                            <img class="col s12" src="<?=$istop==1?"img/top.png":"img/nottop.png"?>" onclick="sendTop(<?=$id_vrau?>, '<?=$_SESSION['login']?>', <?= $istop==1? '0':'1' ?>, this);">
                                            <span class="col s12 center"><?=$topcount?></span>
                                        </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </li>
                            <?php
                        }
                        echo "<script>var lvrau = " . $lvrau . ";</script>";
                        echo "<script>var username = '" . $_SESSION['login'] . "';</script>";
                        ?>

                    </ul>
                   <ul id="callList" class="marginzerot" style="display: none;">
                        <?php
                        
                        $query = "SELECT C.Id_Call, C.Descricao_Call, C.Data_Call, C.imagem, C.nome_call, C.local_call, C.dataPostagem_call, U.nick FROM calls C, usuario U WHERE C.id_Criador = U.Id_User ORDER BY 3;";
                        $stmt = $mysqli->prepare($query);
                        $stmt->execute();
                        $stmt->bind_result($id_call, $texto_call, $data_call, $imagem_call, $nome_call, $local_call, $dataPostagem_call, $nick_criador);
                        $stmt->store_result();

                        while ($stmt->fetch()) {
                            $data_call = $date = date_create($data_call);
                            $data_call = date_format($data_call, 'd/m/Y H:i:s');
                            $mes_num = substr($data_call, 3, 2);

                        ?>
                            <li>
                                <div class="post row">
                                    <div class="contentPost valign-wrapper">
                                        <div class="col s6 valign-wrapper">
                                            <div class="col s3">
                                                <div class="row">
                                                    <span class="col s12" style="font-size: 70px; padding: 0px;"><?= substr($data_call, 0, 2) ?></span>
                                                    <span class="col s12" style="font-size: 30px; margin-top: -30px;">
                                                <?php 
                                                    if($mes_num == "01"){
                                                        echo "JAN";
                                                    }else if($mes_num == "02"){
                                                        echo "FEV";
                                                    }else if($mes_num == "03"){
                                                        echo "MAR";
                                                    }else if($mes_num == "04"){
                                                        echo "ABR";
                                                    }else if($mes_num == "05"){
                                                        echo "MAI";
                                                    }else if($mes_num == "06"){
                                                        echo "JUN";
                                                    }else if($mes_num == "07"){
                                                        echo "JUL";
                                                    }else if($mes_num == "08"){
                                                        echo "AGO";
                                                    }else if($mes_num == "09"){
                                                        echo "SET";
                                                    }else if($mes_num == "10"){
                                                        echo "OUT";
                                                    }else if($mes_num == "11"){
                                                        echo "NOV";
                                                    }else{
                                                        echo "DEZ";
                                                    }   
                                                ?>
                                                </span>
                                                </div>
                                            </div>
                                            <div class="col s9">
                                            <img class="post-img " src="<?= $imagem_call ?>">
                                            </div>
                                        
                                        </div>  
                                        <div class="col s6 valign-wrapper">
                                            <div class="col s12">
                                                <span class="col s12" style="font-size: 18px;"><?= $nome_call ?></span>
                                                <h6 class="col s12" style="font-size: 12px;"> <i class="tiny material-icons" style=" vertical-align: middle;">location_on</i> <?= $local_call ?></h6>
                                            <button class="col s3" style="background-color:white;color: #333;border: none;padding-left: 20px;padding-right: 20px; margin-bottom: 10px;" >VRAU</button>
                                            <a class="col s2" href=""><i class="big material-icons">add</i></a>
                                            </div>
                                        </div>                                       
                                    </div>
                                </div>
                            </li>
                        <?php
                        } 
                        ?>
                    </ul>

                    <ul id="gtList" class="marginzerot" style="display:none">

                        <?php
                        $query = "SELECT G.Id_GT, G.Texto_GT, U.nick, U.nome, U.imagem, G.data_GT, G.imagem FROM gt G, usuario U where U.id_user=G.Id_user order by 1 desc";
                        $stmt = $mysqli->prepare($query);
                        $stmt->execute();
                        $stmt->bind_result($id_gt, $texto_gt, $nick, $nome, $imagem, $data_gt, $imagemGT);
                        $stmt->store_result();


                        $lgt = 0;
                        $fgt = -1;
                        while ($stmt->fetch()) {
                            $data_gt = $date = date_create($data_gt);
                            $data_gt = date_format($data_gt, 'd/m/Y H:i:s');
                            if ($fgt == -1) {
                                $fgt = $id_gt;
                            }
                            if ($fgt > $id_gt) {
                                $fgt = $id_gt;
                            }
                            if ($lgt < $id_gt) {
                                $lgt = $id_gt;
                            }

                            //for($i=0; $i<$texto_gt.length(); $i++ ){

                            //}

                            ?>
                            <li>
                                <div class="post row">
                                    <div class="gg col s2 center" ><img class="img-post" src="<?php echo $imagem; ?>" alt="" /></div>
                                    <div class="contentPost col s10 m10 l10">
                                        <p>@<?= $nick ?> (<?= $nome ?>)<span class="data"> - <?= $data_gt ?></span></p> 
                                        <span class="conteudo" id="contentP" style="color: white; font-family: 'Lucida Console';font-size: 14px;">
                                            <div style="border-left: 2px solid #ddd; padding-left: 10px;"><?= $texto_gt ?><br></div>
                                            <?php 
                                                if(!is_null($imagemGT)){
                                                    echo "<a href='".$imagemGT."' target='_blank'><img src='".$imagemGT."' style='width: auto;'></a>";
                                                }
                                            ?>
                                            <br><br>
                                        </span>
                                    </div>
                                </div>
                            </li>

                            <?php
                        }
                        echo "<script>var lvrau = " . $lvrau . ";</script>";
                        ?>

                    </ul>

                </div>
            </div>
        </div>    
        
    </body>
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="js/materialize.js"></script>
        <script src="js/init.js"></script>
        <script src="js/custom.js"></script>
        <script>
            $(document).ready(function(){
                $('.materialboxed').materialbox();
            });
            
            $( "input[name='tipo']" ).click(function (){
                if( $("#test3").is(':checked')){
                    $("#textarea1").val(">");
                }else{
                    $("#textarea1").val("");
                }
            });
            $("#textarea1").keyup(function ( event){
                if( $("#test3").is(':checked')){
                        if(event.which == 13){
                            var $texto = $("#textarea1").val();
                            $("#textarea1").val($texto+">");
                        }
                }
            });
            
            var outputHTMLVrau = "";
            var outputHTMLOld = "";
            var newVrauCont = 0;

            var username = "<?=$_SESSION['login']?>";

            $(function () {
                $("#newVrauAlert").hide();
                setInterval(newvraus, 60000);
                //alert('hi');
            });
            console.log(lvrau+" "+ username);
            function newvraus() {
                $.ajax({
                    type: "POST",
                    url: "ajaxfunctions/newfeed.php",
                    dataType: 'json',
                    data: {
                        lvrau: lvrau,
                        username: username,
                        action: "getnews"
                    },
                    success: function (result) {

                        //alert("Resultado Ajax");
                        newVrauCont += result.length;

                        outputHTMLOldVrau = outputHTMLVrau;
                        //alert(newVrauCont);
                        //alert(result);
                        if (newVrauCont > 0) {
                            document.title = "Maquiné(" + newVrauCont + ")";
                            //alert("Dentro if");
                            //var tweets = twitterFeed.statuses;
                            // Begin HTML wrapper

                            $("#newVrauAlertText").text(newVrauCont + ' novos vraus');
                            $("#newVrauAlert").show();

                            outputHTMLVrau = "";

                            // Loop through JSON data and populate into HTML list
                            for (var i = 0; i < result.length; i++) {
                                outputHTMLVrau += '<li>';
                                outputHTMLVrau += '<div class="post row">';
                                // Declare variables for required data
                                var vrauId = result[i].id;
                                var vrauText = result[i].text;
                                vrauText = vrauText.replace('>', '&rsaquo;');
                                vrauText = vrauText.replace('<', '&lsaquo;');
                                var vrauNick = result[i].nick;
                                var vrauName = result[i].nome;
                                var vrauImg = result[i].imagem;
                                var vrauDate = result[i].data;
                                var vrauImgV = result[i].imagemvrau;
                                var vrauTopCount = result[i].topcount;
                                var vrauistop = result[i].istop;

                                if (i == 0) {
                                    console.log(vrauId);
                                    lvrau = vrauId;
                                }
                                // HTML magic
                                outputHTMLVrau += '<div class="col s2 center" ><img class="img-post" src="' + vrauImg + '" alt="" /></div>';
                                outputHTMLVrau += '<div class="contentPost col s10 m10 l10">';
                                outputHTMLVrau += '<div class="row valign-wrapper">';
                                outputHTMLVrau += '<div class="col s11">';
                                outputHTMLVrau += '<p>@' + vrauNick + ' (' + vrauName + ')<span class="data"> - ' + vrauDate + '</span></p>';
                                outputHTMLVrau += '<span class="conteudo row" id="contentP">';
                                outputHTMLVrau += '<div class="col s12">';
                                outputHTMLVrau += vrauText;
                                outputHTMLVrau += '</div>';
                                //alert(vrauImgV);
                                if(vrauImgV!==''){
                                    outputHTMLVrau += '<div class="col s12">';
                                    outputHTMLVrau += '<img class="materialboxed" src="'+vrauImgV+'" style="width: auto;">';
                                    outputHTMLVrau += '</div>';
                                }
                                outputHTMLVrau += '</span>';
                                outputHTMLVrau += '</div>';
                                
                                outputHTMLVrau += '<div class="col s1" style="height: 100%">';
                                var src ="";
                                if(vrauistop ==1){
                                    src= "img/top.png";
                                }else{
                                    src= "img/nottop.png";
                                }
                                username = "'"+username+"'";
                                outputHTMLVrau += '<img class="col s12" src="'+src+'" onclick="sendTop('+vrauId+', '+username+', 0, this);">';
                                
                                outputHTMLVrau += '<span class="col s12 center">'+vrauTopCount+'</span>';
                                outputHTMLVrau += '</div>';
                                
                                outputHTMLVrau += '</div>';
                                outputHTMLVrau += '</div>';
                                outputHTMLVrau += "</li>";

                            }
                            outputHTMLVrau += outputHTMLOldVrau;

                            //$("#vrauList").prepend(outputHTML);
                        } else {
                            document.title = "Maquiné";
                        }
                    }
                });
            }
            $("#newVrauAlert").click(function () {
                //alert("Disparou evento click");
                $("#vrauList").prepend(outputHTMLVrau);
                $("#newVrauAlert").hide();
                newVrauCont = 0;
                outputHTMLOldVrau = "";
                outputHTMLVrau = "";
                document.title = "Maquiné";
            });

            if ($('#back-to-top').length) {
                var scrollTrigger = 100, // px
                        backToTop = function () {
                            var scrollTop = $(window).scrollTop();
                            if (scrollTop > scrollTrigger) {
                                $('#back-to-top').addClass('show');
                            } else {
                                $('#back-to-top').removeClass('show');
                            }
                        };
                backToTop();
                $(window).on('scroll', function () {
                    backToTop();
                });
                $('#back-to-top').on('click', function (e) {
                    e.preventDefault();
                    $('html,body').animate({
                        scrollTop: 0
                    }, 700);
                });
            }

            $("#busca").on('input', function () {
                console.log($("#busca").val());
                busca($("#busca").val());
            });
            function busca(termobusca) {
                console.log(termobusca);
                termobusca = termobusca.replace('@', '');
                var htmlBusca = "";
                if (termobusca != "") {
                    $.ajax({
                        type: "POST",
                        url: "ajaxfunctions/search.php",
                        dataType: 'json',
                        data: {
                            busca: termobusca
                        },
                        success: function (result) {
                            //alert("Dentro if");
                            //var tweets = twitterFeed.statuses;
                            // Begin HTML wrapper

                            htmlBusca = "";

                            // Loop through JSON data and populate into HTML list
                            for (var i = 0; i < result.length; i++) {
                                htmlBusca += '<li><div class="row">';
                                // Declare variables for required data
                                var vrauNickB = result[i].nick;
                                var vrauNameB = result[i].nome;
                                var vrauImgB = result[i].imagem;

                                // HTML magic
                                htmlBusca += '<div class="col s3"><img src="' + vrauImgB + '" class="post-img"></div>';
                                htmlBusca += '<div class="col s9"><div class="row">';
                                htmlBusca += '<div class="col s12"><span>' + vrauNameB + '</span></div>';
                                htmlBusca += '<div class="col s12"><span>@' + vrauNickB + '</span></div>';
                                htmlBusca += '</div></div>';
                                htmlBusca += '</div></li>';
                                htmlBusca += '<li class="divider"></li>';

                            }

                            console.log(htmlBusca);
                            $("#dropdown2").html(htmlBusca);

                        }
                    });
                }else{
                    $("#dropdown2").html(htmlBusca);
                };
            }     
            
             function cabritinha(){ //função pra setar enable ou disable o text ara quando selecionado os radioButtons
                if (document.getElementById('test1').checked){
                    bode(100);
                    document.getElementById('vrauList').style.display = "block";
                    document.getElementById('callList').style.display = "none";
                    document.getElementById('gtList').style.display = "none";
                    document.getElementById('noncall').style.display = "block";
                    document.getElementById('callInputs').style.display = "none";
                    $(".chupaminharola").prop('required',false);
                    $("#textarea1").prop('required', true);
                }
                else if(document.getElementById('test2').checked){
                    document.getElementById('noncall').style.display = "none";
                    //alert('Call ainda não está disponivel!');
                    bode(0);
                    document.getElementById('vrau').disabled;
                    document.getElementById('callList').style.display = "block";
                    document.getElementById('vrauList').style.display = "none";
                    document.getElementById('gtList').style.display = "none";
                    document.getElementById('callInputs').style.display = "block";
                    $(".chupaminharola").prop('required',true);
                    $("#textarea1").prop('required', false);
                }
                else if(document.getElementById('test3').checked){
                    bode("");
                    document.getElementById('gtList').style.display = "block";
                    document.getElementById('vrauList').style.display = "none";
                    document.getElementById('callList').style.display = "none";
                    document.getElementById('noncall').style.display = "block";
                    document.getElementById('callInputs').style.display = "none";
                    $(".chupaminharola").prop('required',false);
                    $("#textarea1").prop('required', true);
                }
                else{
                    alert('ERRO');
                }
            }
            function bode(issoaiparca){ //só pra não ficar repetitivo na cabritinha
                document.getElementById('textarea1').setAttribute('length',issoaiparca);
                document.getElementById('textarea1').setAttribute('maxlength',issoaiparca);
            }

        </script>
        
        <script>
            //$('img').click();
            
            function sendTop($vrauid, $userid, $option, $img) {
                
                //console.log("dsadsa");
                console.log($($img).prop('src'));
                var $src = $($img).prop('src');
                if($src.indexOf("img/top.png") ==-1){
                    $option =1;
                    $($img).attr("src", "img/top.png");
                    //$($img).attr("onclick","sendTop("+$vrauid+", "+ $userid+", "+ 0 +", "+ $img+")");
                    console.log($($($($img).closest("div")).find("span")).text());
                    var $top = $($($($img).closest("div")).find("span")).text();
                    $($($($img).closest("div")).find("span")).text(parseInt($top)+1);
                }else{
                    $option =0;
                    $($img).attr("src", "img/nottop.png");
                    //$($img).attr("onclick","sendTop("+$vrauid+", "+ $userid+", "+ 1 +", "+ $img+")");
                    var $top = $($($($img).closest("div")).find("span")).text();
                    $($($($img).closest("div")).find("span")).text(parseInt($top)-1);
                }
                
                $.ajax({
                    type: "POST",
                    url: "ajaxfunctions/newfeed.php",
                    dataType: 'json',
                    data: {
                        vrauid: $vrauid,
                        userid: $userid,
                        option: $option,
                        action: "sendtop"
                    },
                    success: function (result) {
                    }
                });
            }
        
        </script>
</html>
<?php
// tira o resultado da busca da memória
$stmt->close();
?>